local aspect_ratio = require('lib/aspect_ratio')
local canvas
function love.load()
    local w, h = love.graphics.getDimensions()
    aspect_ratio:init(w, h, 320, 240)
    canvas = love.graphics.newCanvas(aspect_ratio.dig_w, aspect_ratio.dig_h)
end
function love.update(dt)
    canvas:renderTo(
        function()
            love.graphics.clear(255, 0, 0, 255)
            love.graphics.rectangle('fill', 10, 10, 40, 100)
        end
    )
end
function love.draw()
    love.graphics.clear()
    love.graphics.draw(canvas, aspect_ratio.x, aspect_ratio.y, 0, aspect_ratio.scale)
end
function love.resize(w, h)
end
